import { UserAddressModel } from "./user-address.model";
import { UserCompanyModel } from "./user-company.model";

export class UserModel {
    public id = 0;
    public name = '';
    public username = '';
    public email = '';
    public address?: UserAddressModel;
    public phone = '';
    public website = '';
    public company?: UserCompanyModel;
}