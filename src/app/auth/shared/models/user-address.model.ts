import { UserAddressGeoModel } from "./user-address-geo.model";

export class UserAddressModel {
    public street?: string;
    public suite?: string;
    public city?: string;
    public zipcode?: string;
    public geo?: UserAddressGeoModel;
}