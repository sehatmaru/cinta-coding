import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { UserModel } from '../models/user.model';

const baseURL = 'https://jsonplaceholder.typicode.com';

@Injectable({
	providedIn: 'root',
})
export class AuthService {
	constructor(
		private http: HttpClient,
	) {}

	public getUsers(): Observable<UserModel[]> {
		const headers = new HttpHeaders({
			'Content-Type': 'application/json',
		});

		return this.http.get<UserModel[]>(`${baseURL}/users`);
	}

	public getUserDetail(userId: number): Observable<UserModel> {
		const headers = new HttpHeaders({
			'Content-Type': 'application/json',
		});

		return this.http.get<UserModel>(`${baseURL}/users/${userId}`);
	}
}
