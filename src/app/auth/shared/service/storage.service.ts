import { Injectable } from '@angular/core';

const ID = 'account.id';
const USERNAME = 'account.username';
const NAME = 'account.name';

@Injectable({
	providedIn: 'root',
})
export class StorageService {
	public setLogin(id: number, username: string, name: string) {
		localStorage.setItem(ID, id + '');
		localStorage.setItem(USERNAME, username);
		localStorage.setItem(NAME, name);
	}

	public removeLogged() {
		localStorage.removeItem(ID);
		localStorage.removeItem(USERNAME);
		localStorage.removeItem(NAME);
	}

	public isLogged() {
		return localStorage.getItem(USERNAME) != null;
	}

	public getUsername() {
		return localStorage.getItem(USERNAME) ?? '';
	}

	public getId() {
		return localStorage.getItem(ID) ?? '';
	}

	public getName() {
		return localStorage.getItem(NAME) ?? '';
	}
}
