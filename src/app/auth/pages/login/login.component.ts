import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../../shared/service/auth-api.service';
import { StorageService } from '../../shared/service/storage.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public submitted = false;
  public notFound = true;
  public isLoading = false;

	public loginForm = new FormGroup(
    {
      username: new FormControl(Validators.required),
      password: new FormControl(Validators.required)
    }
  );

  get f(){
    return this.loginForm.controls;
  }

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private storageService: StorageService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.loginForm = this.formBuilder.group(
      {
        username: ['', Validators.required],
        password: ['', Validators.required],
      },
    );
  }

  public onFinish() {
    this.submitted = true;
    
    if (!this.loginForm.invalid) {
      this.isLoading = true;
      
      this.authService.getUsers()
        .subscribe((resp) => {
          for (const user of resp) {
            if (user.username === this.f['username'].value){
              this.storageService.setLogin(user.id, user.username, user.name);
              this.notFound = false;
              this.router.navigate([''])
            }
          }

          this.isLoading = false;
        }
      );
    }
  }

  public resetAuth() {
    this.submitted = false;
  }
}
