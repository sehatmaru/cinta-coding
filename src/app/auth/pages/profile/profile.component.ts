import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserModel } from '../../shared/models/user.model';
import { AuthService } from '../../shared/service/auth-api.service';
import { StorageService } from '../../shared/service/storage.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  public isLoading = false
  public user?: UserModel;

  constructor(
    private router: Router,
    private storageService: StorageService,
    private authService: AuthService,
    private location: Location
  ) { }

  ngOnInit(): void {
    this.getUserDetail();
  }

  private getUserDetail() {
    this.isLoading = true

    this.authService.getUserDetail(Number(this.storageService.getId())).subscribe(resp => {
      if (resp) {
        this.user = resp;
      }

      this.isLoading = false
    })
  }

  public toPrevPage() {
    this.location.back();
  }
}
