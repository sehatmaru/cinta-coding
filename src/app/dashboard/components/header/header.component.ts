import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { StorageService } from 'src/app/auth/shared/service/storage.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  public isLogged?: boolean
  public name?: string

  constructor(
    private storageService: StorageService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.isLogged = this.storageService.isLogged()
    this.name = this.storageService.getName().split(" ")[0]
  }

  public toLogin() {
    this.router.navigate(['login'])
  }

  public toProfile() {
    this.router.navigate(['profile'])
  }

  public toDashboard() {
    this.router.navigate([''])
  }
}
