import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CommentModel } from '../../shared/models/comment.model';
import { PostModel } from '../../shared/models/post.model';

@Component({
  selector: 'app-post-card',
  templateUrl: './post-card.component.html',
  styleUrls: ['./post-card.component.scss']
})
export class PostCardComponent implements OnInit {

  @Input()
  public post?: PostModel;

  @Input()
  public comments?: CommentModel[];

  @Input()
  public isDetail?: boolean;

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  public toDetail() {
    this.router.navigate(['post/detail'], {
			queryParams: {
				id: this.post?.id
			},
		});
  }
}
