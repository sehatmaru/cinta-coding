import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { PostListComponent } from './pages/post-list/post-list.component';
import { PostCardComponent } from './components/post-card/post-card.component';
import { HeaderComponent } from './components/header/header.component';
import { HttpClientModule } from '@angular/common/http';
import { MaterialModule } from '../material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PostDetailComponent } from './pages/post-detail/post-detail.component';


@NgModule({
  declarations: [
    PostListComponent,
    PostCardComponent,
    HeaderComponent,
    PostDetailComponent
  ],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    MaterialModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule
  ],
  exports: [
    HeaderComponent
  ]
})
export class DashboardModule { }
