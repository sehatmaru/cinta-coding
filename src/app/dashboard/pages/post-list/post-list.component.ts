import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UserModel } from 'src/app/auth/shared/models/user.model';
import { AuthService } from 'src/app/auth/shared/service/auth-api.service';
import { StorageService } from 'src/app/auth/shared/service/storage.service';
import { PostModel } from '../../shared/models/post.model';
import { PostService } from '../../shared/service/post-api.service';

@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.scss']
})
export class PostListComponent implements OnInit {

  public value = ''
  public isLogged = false
  public submitted = false
  public isLoading: boolean[] = []
  public postList: PostModel[] = []
  public users: UserModel[] = []
  public currentPost: PostModel[] = []

  public currentPage = 1
  public totalPage = 0

	public searchForm = new FormGroup({search: new FormControl()})

  get f(){
    return this.searchForm.controls
  }

  constructor(
    private storageService: StorageService,
    private postService: PostService,
    private authService: AuthService
  ) { }

  ngOnInit(): void {
    this.isLogged = this.storageService.isLogged()

    this.getPostList();
  }

  private getPostList() {
    this.isLoading.push(true);

    this.postService.getPostList().subscribe(resp => {
      if (resp) {
        this.postList = resp
        this.totalPage = Math.round(resp.length/10)

        this.setCurrentPost()
        this.getUsers();
      }

      this.isLoading.pop()
    });
  }

  private getUsers() {
    this.isLoading.push(true);
    
    this.authService.getUsers().subscribe(resp => {
      if (resp) {
        this.users = resp;

        this.setNameAndComments();
      }

      this.isLoading.pop()
    })
  }

  public search() {
    console.log(this.f['search'].value)
  }

  public prevPage() {
    if (this.currentPage !== 1) {
      this.currentPage -= 1
      this.setCurrentPost()
    }
  }

  public nextPage() {
    if (this.currentPage !== this.totalPage) {
      this.currentPage += 1
      this.setCurrentPost()
    }
  }

  public selectPage(page: number) {
    this.currentPage = page
    this.setCurrentPost()
  }

  private setCurrentPost() {
    this.currentPost = [];
    const length = this.currentPage*10

    for (let i=length-10; i<length; i++) {
      this.currentPost?.push(this.postList[i])
    }
  }

  private setNameAndComments() {
    for (const post of this.postList) {
      this.isLoading.push(true);
    
      for (const user of this.users) {
        if (post.userId === user.id) {
          post.name = user.name;
          break;
        }
      }

      this.postService.getComments(post.id).subscribe(resp => {
        if (resp) {
          post.totalComment = resp.length;
        }

        this.isLoading.pop();
      })
    }
  }
}
