import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserModel } from 'src/app/auth/shared/models/user.model';
import { AuthService } from 'src/app/auth/shared/service/auth-api.service';
import { CommentModel } from '../../shared/models/comment.model';
import { PostModel } from '../../shared/models/post.model';
import { PostService } from '../../shared/service/post-api.service';

@Component({
  selector: 'app-post-detail',
  templateUrl: './post-detail.component.html',
  styleUrls: ['./post-detail.component.scss']
})
export class PostDetailComponent implements OnInit {

  public isLoading: boolean[] = []
  public post: PostModel = new PostModel()
  public users: UserModel[] = []
  public comments: CommentModel[] = []

  private postId?: number

  constructor(
    private postService: PostService,
    private authService: AuthService,
		private activatedRoute: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit(): void {
		this.postId = this.activatedRoute.snapshot.queryParams['id']
    
    this.getUsers()
    this.getPostDetail()
  }

  private getPostDetail() {
    if (this.postId) {
      this.isLoading.push(true)
      
      this.postService.getPostDetail(this.postId).subscribe(resp => {
        if (resp) {
          this.post = resp
        }

        if (this.isLoading.length === 1) this.setNameAndComments()
        else this.isLoading.pop()
      })
    }
  }

  private getUsers() {
    this.isLoading.push(true)

    this.authService.getUsers().subscribe(resp => {
      if (resp) {
        this.users = resp
      }

      if (this.isLoading.length === 1) this.setNameAndComments()
      else this.isLoading.pop()
    })
  }

  private setNameAndComments() {
    for (const user of this.users) {
      if (this.post.userId === user.id) {
        this.post.name = user.name
        break
      }
    }

    this.postService.getComments(this.post.id).subscribe(resp => {
      if (resp) {
        this.post.totalComment = resp.length
        this.comments = resp;
      }

      this.isLoading.pop()
    })
  }

  public toList() {
    this.router.navigate([''])
  }
}
