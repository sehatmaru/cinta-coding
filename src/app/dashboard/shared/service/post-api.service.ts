import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CommentModel } from '../models/comment.model';
import { PostModel } from '../models/post.model';

const baseURL = 'https://jsonplaceholder.typicode.com';

@Injectable({
	providedIn: 'root',
})
export class PostService {
	constructor(
		private http: HttpClient,
	) {}

	public getPostList(): Observable<PostModel[]> {
		const headers = new HttpHeaders({
			'Content-Type': 'application/json',
		});

		return this.http.get<PostModel[]>(`${baseURL}/posts`);
	}

	public getPostDetail(postId: number): Observable<PostModel> {
		const headers = new HttpHeaders({
			'Content-Type': 'application/json',
		});

		return this.http.get<PostModel>(`${baseURL}/posts/${postId}`);
	}

	public getComments(postId: number): Observable<CommentModel[]> {
		const headers = new HttpHeaders({
			'Content-Type': 'application/json',
		});

		return this.http.get<CommentModel[]>(`${baseURL}/posts/${postId}/comments`);
	}
}
