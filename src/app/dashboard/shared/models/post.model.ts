export class PostModel {
    public id = 0
    public userId = 0
    public name = ''
    public title = ''
    public body = ''
    public totalComment = 0
}