# Cinta Coding

This is Web Project generated with Angular CLI version 13.2.2 only for Application Test only.

## Tech Stack

Angular 13, Typescript, Angular Material, Reactive Forms

## How To Run Locally

Clone the project

```
  git clone https://gitlab.com/sehatmaru/cinta-coding
```

Go to the project directory

```
  cd my-project
```

Install dependencies

```
  npm install
```

Start the server

```
  ng serve
```

Open browser

```
  http://localhost:4200/
```

## Project Structure

```
app
└── feature
    ├── components
    │    └── components-1
    │        ├── components-1.component.html
    │        ├── components-1.component.scss
    │        └── components-1.component.ts
    ├── pages
    |   └── pages-1
    │        ├── pages-1.component.html
    |        ├── pages-1.component.scss
    │        └── pages-1.component.ts
    ├── shared
    |    ├── models
    │    │   └── model-1.model.ts
    |    └── service
    │        └── service-1.model.ts
    ├── feature-routing.module.ts
    └── feature.module.ts
```
